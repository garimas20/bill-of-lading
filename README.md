import os
import time
import json
from urllib.request import urlopen
from datetime import date, datetime
import requests

# from flask import Flask, Blueprint, jsonify, request, abort, send_file
import schedule
from schedule import repeat,every,run_pending
# from pandas.io.json import json_normalize
from requests.auth import HTTPBasicAuth
import string
import random
import base64
log_path = os.getcwd()+"/app/logs/"

# app=Flask(__name__)



today = date.today()

# to schedule the download at 10 everyday
def cronjob():
  try:
      print("IN cron job")
      schedule.every(2).minutes.do(searchfn)

      #schedule.every().day.at('10:00').do(searchfn)

      while True:
          run_pending()
          time.sleep(1)
  except Exception as e:
        print ('Generic Exception' + str(e))

def searchfn():
  try:
    print("IN search func")
    global fname
    url = "https://api.aws-uat.idfcfirstbank.com/docs-exp/v1/searchDocuments"
    # generating random msgId
    msgId = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(14))
    print("msgId",msgId)

    timestamp = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    print("timestamp",timestamp)
    python_log_func("Wrking in cron in search")
    # the given values of FEMA, DPD and CPDD

    file_dict={'FEMA':{'indxId': '47', 'indxVal':'FEMA Overdue Framework Report ', 'dateDefIndx':'22'},
    'CPDD':{'indxId': '47', 'indxVal':'CPDD Report', 'dateDefIndx':'22'},
    'DPD':{'indxId': '47', 'indxVal':'Overdue Framework Report', 'dateDefIndx':'22'}}

    file_list=['FEMA','CPDD','DPD']
    # loop to search for all the files
    for i in file_list:
      print("file",i)
      payload = json.dumps({
          "searchDocumentsReq": {
            "msgHdr": {
               "frm": {
                  "id": "SFDC"
                  },
              "hdrFlds": {
                "cnvId": msgId,
                "msgId": msgId,
                "timestamp": timestamp
              },

            },
            "msgBdy": {
              "crtnDtRng": "BETWEEN,2023-07-07,2023-09-09",
              "nm": "",
              "noOfRcrdsToFtch": "10",
              "ordrBy": "5",
              "prntFldrIndx": "3670",
              "dataDefCriterion": {
                "dataDefCriteria": [
                  {
                    "dataDefIndx": file_dict[i]['dateDefIndx'],
                    "indxId": file_dict[i]['indxId'],
                    "indxVal": file_dict[i]['indxVal'],
                    "joinCndtin": "AND",
                    "oprtr": "="
                  }
                ]
              },
              "srtOrdr": "D",
              "srcNm":"UAT_TRADE_RAPID"
            }
          }
          }
        )
      headers = {
          'correlationId': 'e0c4a3c2-d29d-11eb-b8bc-0242ac130003',
          'source': 'ACS',
          'Content-Type': 'application/json',
          'Authorization': 'Basic ZmFjNTE5NDgtYmMwNi00Y2U1LTk2NTItOTUxOGNjYjJiNDQwOjlzemlxeURUSzR0U3FYRkVWVTExTEx1UFhp'
        }

      response_search = requests.request("POST", url, headers=headers, data=payload,verify=False) #,auth=HTTPBasicAuth('877d6402-944d-48ec-8326-02670ec330c3', 'Yxkn4Jj2OjAcBeFr53omb0gvXm'))
      response_search_js = response_search.json()
      print("Response of search api ")
      python_log_func(response_search_js)
      python_log_func("IN DOWNLOAD")
      fname=i
      print("file",i)

      try:
        docIndx=response_search_js['searchDocumentsResp']['msgBdy']['srchRsltDataBDO']
        docInd=docIndx[::-1]
        for doc in docInd:
            python_log_func(doc)
            dcIndxx=doc['docIndx']
            filetype=doc['crtdByAppNm']
            crtnDtTm=doc['crtnDtTm']
            docName=doc['docNm']
            if filetype =='xlsx':
                print("DocIndx",dcIndxx)
                download(dcIndxx,crtnDtTm,docName)

        python_log_func("WRking in cron after search")

      except Exception as e:
         fail_log_func(str(e))
         python_log_func("error in search"+str(e))
         continue

  except Exception as e:
        print ('Generic Exception' + str(e))
        fail_log_func(str(e))

def download(docIndx,crtnDtTm,docName):

    print("Download function")

    curr_datestamp= today.strftime("%d_%m_%Y")
    req_dcNm = f'{fname}_{curr_datestamp}'
    print("req_ dcname", req_dcNm)

    url="https://api.aws-uat.idfcfirstbank.com/docs-exp/v1/downloadDoc"
    try:
        payload= json.dumps({
                    "downloadDMSDocReq": {
                        "msgHdr": {
                            "frm": {
                                "id": "RIB"
                            },
                            "hdrFlds": {
                                "cnvId": "3012 P01",
                                "msgId": "3012P01",
                                "timestamp": "2019-12-24T19:55:41.187+05:30"
                            }
                        },
                        "msgBdy": {
                            "dcIndx": docIndx,
                            "dcNm": req_dcNm
                        }
                    }
                }
                )
        headers = {
          'correlationId': 'e0c4a3c2-d29d-11eb-b8bc-0242ac130003',
          'source': 'ACS',
          'Authorization': 'Basic ZmFjNTE5NDgtYmMwNi00Y2U1LTk2NTItOTUxOGNjYjJiNDQwOjlzemlxeURUSzR0U3FYRkVWVTExTEx1UFhp',
          'Content-Type': 'application/json'
        }
        response_download= requests.request('POST',url,headers=headers,data=payload,verify=False) #auth=HTTPBasicAuth('fac51948-bc06-4ce5-9652-9518ccb2b440', '9sziqyDTK4tSqXFEVU11LLuPXi'))

        response_downloadjs = response_download.json()
        try:
          response_file= response_downloadjs['downloadDMSDocResp']['msgBdy']['bs64Doc']
          print("Getting response file")
          python_log_func("IN RESPONSE")
          # Based on the required fname, calling the api
          if fname=='FEMA':
            python_log_func("FEMA")
            try:
              python_log_func("in fema")
              url = "https://simbaimages.idfcbank.com/api/fema/add"
              payload=json.dumps({ "file":str(response_file),"crtDt":str( crtnDtTm ),"dcName":str(docName)})
              headers = {'Content-Type':'application/json'}

              response = requests.request("POST", url, headers=headers, data=payload,verify=False)
              python_log_func(response)
              print("File uploded")
            except Exception as e:
              fail_log_func(str(e))
              print ('Generic Exception' + str(e))

          if fname=='DPD':
            python_log_func("dpp")
            try:
              python_log_func("in dpd")
              url = "https://simbaimages.idfcbank.com/api/dpd/add"
              payload=json.dumps({ "file":str( response_file),"crtDt":str( crtnDtTm),"dcName":str(docName)})
              headers = {'Content-Type':'application/json'}
              response = requests.request("POST", url, headers=headers, data=payload,verify=False)
              #python_log_func(response)
              #print("DPD response",response)
              python_log_func("update dpd")
              print("File uploded")

            except Exception as e:
              fail_log_func(str(e))
              print ('Generic Exception' + str(e))

          if fname=='CPDD':
            python_log_func("cpdd")
            try:
              url = "https://simbaimages.idfcbank.com/api/cpdd/add"
              payload=json.dumps({ "file":str( response_file),"crtDt":str(crtnDtTm),"dcName":str(docName)})
              headers = {'Content-Type':'application/json'}
              response = requests.request("POST", url, headers=headers, data=payload,verify=False)
              python_log_func(response)
              print("request of cpdd",payload)
              #print("CRPDD response",response)
              python_log_func("updated cpdd")
              print("File uploded")

            except Exception as e:
              fail_log_func(str(e))
              print ('Generic Exception' + str(e))

        except Exception as e:
              fail_log_func(str(e))



    except Exception as e:
        print ('Generic Exception' + str(e))
        fail_log_func(str(e))

def python_log_func(message):
    now = datetime.now()
    # print("Request ID for python logs :", request_id)
    print("now=", now)
    today = date.today()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    # dt_start_string = start.strftime("%d/%m/%Y %H:%M:%S")
    print("date and time =", dt_string)
    strs_d = {"message":message , "Time": dt_string}
    with open(log_path+"python_trade"+str(today)+".txt", "a+") as fh:
        fh.write("\n")
        fh.write(str(strs_d))

def fail_log_func(error):
    now = datetime.now()
    # print("Request ID for python logs :", request_id)
    print("now=", now)
    today = date.today()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    # dt_start_string = start.strftime("%d/%m/%Y %H:%M:%S")
    print("date and time =", dt_string)
    strs_d = {"UCIC": "going", "error is : ": error , "Time": dt_string}
    with open(log_path+"else_in_exposure"+str(today)+".txt", "a+") as fh:
        fh.write("\n")
        fh.write(str(strs_d))
# if __name__ == '__main__':
  # app.run(debug=True,host='0.0.0.0', port=5432)
cronjob()
print("Upload done")
